## spiralize ##


R package. Pretty (?) visualization of (time)series.

### To instal it ###

      # install.packages("devtools")
      library(devtools)
      install_bitbucket("spiralize","8qube")

(Windows users must have [RTools](http://cran.r-project.org/bin/windows/Rtools/ "Rtools") installed)

![gif](http://8qu.be/spiral.gif)

### Sample plot ###

[![spiralize](http://8qb.org/img/spiralize.png)](http://8qb.org/img/spiralize.gif)

